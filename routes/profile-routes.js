const router=require('express').Router();
 
const authCheck=(req,res,next)=>{
	if(!req.user){
		//if user not login
		res.redirect('/auth/login');
	}else{
		//if login 
		next();
	}
};

 router.get('/',(req,res)=>{
 	res.render('profile',{user:req.user});
 });
 module.exports=router;