const mongoose=require('mongoose');
const Schema = mongoose.Schema;

const userSchema=new Schema({
	username:String,
	googleId:String,//so when user login second time he/ she will be identified.
	thumbnail:String

});

const User = mongoose.model('user',userSchema);
 
 module.exports=User;